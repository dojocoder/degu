var express = require('express');
var app = express();
var port = 777;

function setupHeaders(req, res, next) {
    next();
}

app.use(setupHeaders);
app.configure(function () {
    app.use(express.methodOverride());
    app.use(express.bodyParser());
    app.use('/', express.static(__dirname));
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
    app.use(app.router);
});

app.listen(port);