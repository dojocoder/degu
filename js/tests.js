describe('DEGU', function () {

    describe('DOM', function () {

        it('query', function(){
            var selector = '.spanNode em',
                parent = 'domSandbox',
                dom = degu.DOM,
                nodes = dom.query(selector, parent);
            nodes[0].should.not.be.undefined;
        });

    });

    describe('PubSub', function () {

        it('возможно создать канал сообщений', function () {
            var cb = sinon.spy(),
                channel1 = 'channel1',
                channel2 = 'channel2';

            var ps = degu.PubSub();
            ps.subscribe(channel1, cb);
            ps.publish(channel1);
            ps.publish(channel1);
            ps.publish(channel2);
            cb.calledTwice.should.be.eql(true);
        });

        it('возможно удалить канал сообщений', function () {
            var cb = sinon.spy(),
                channel = 'channel',
                handle;

            var ps = degu.PubSub();
            handle = ps.subscribe(channel, cb);
            ps.publish(channel);
            ps.unsubscribe(handle);
            ps.publish(channel);
            cb.calledOnce.should.be.eql(true);
        });

        it('возможна передача payload-данных', function () {
            var cb = sinon.spy(),
                channel = 'channel',
                testData = { str: 'Hello World !' };

            var ps = degu.PubSub();
            ps.subscribe(channel, cb);
            ps.publish(channel, testData);
            cb.calledWith(testData).should.be.eql(true);
        });

    });

    describe('Functional', function () {

        it('compose', function () {
            var foo = function () {
                    return 'foo';
                },
                bar = function (str) {
                    return str + 'bar';
                },
                baz = function (str) {
                    return str + 'baz';
                };

            var composition = degu.compose(foo, bar, baz);
            composition('').should.be.eql('foobarbaz');
        });

        it('memoize', function () {
            var stub = sinon.stub(),
                ret = 'value';
            stub.returns(ret);

            function calculation() {
                return stub();
            }

            var memoized = degu.memoize(calculation);
            memoized().should.be.eql(ret);
            memoized().should.be.eql(ret);
            stub.calledOnce.should.be.eql(true);
        });

        it.skip('debounce', function () {

        });

        it.skip('throttle', function () {

        });

        it.skip('partial', function () {

        });

    });


});