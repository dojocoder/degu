window.degu = (function () {
    "use strict";
    var degu;


    function SinglePage(opts) {
        if (!(this instanceof SinglePage)) {
            return new SinglePage(opts);
        }
    }

    SinglePage.prototype = {
        constructor: SinglePage
    };

    function PubSub() {
        if (!(this instanceof PubSub)) {
            return new PubSub();
        }
        this._channels = {};
    }

    PubSub.prototype = {
        constructor: PubSub,
        subscribe: function (channel, cb) {
            if (!this._channels[channel]) {
                this._channels[channel] = [];
            }
            var idx = this._channels[channel].push(cb) - 1;
            return [channel, idx];
        },
        publish: function (channel, data) {
            if (!this._channels[channel]) {
                return;
            }
            this._channels[channel].forEach(function (cb) {
                cb(data);
            });
        },
        unsubscribe: function (handle) {
            this._channels[handle[0]].splice(handle[1], 1);
        }
    };

    function compose() {
        var functions = [].slice.call(arguments);
        if (functions.length) {
            return function () {
                var result = functions[0].apply(null, arguments),
                    i = 1;
                while (i < functions.length) {
                    result = functions[i].call(null, result);
                    i++;
                }
                return result;
            }
        }
    }

    function memoize(fn) {
        var cache = {};
        return function () {
            var memKey = JSON.stringify([].slice.call(arguments));
            return cache.hasOwnProperty(memKey) ? cache[memKey] : cache[memKey] = fn.apply(null, arguments);
        }
    }

    function partial(fn) {
        var args = [].slice.call(arguments, 1);
        return function () {
            var innerArgs = args.concat([].slice.call(arguments));
            return fn.apply(null, innerArgs);
        }
    }

    function debounce(fn, delay, ctxt) {
        var timer;
        return function () {
            clearTimeout(timer);
            ctxt = ctxt || this;
            var args = [].concat.apply([ctxt], arguments);
            timer = setTimeout(fn.bind.apply(fn, args), delay);
        }
    }

    function throttle(fn, threshold, ctxt) {
        threshold = threshold || 250;
        var last,
            timer;

        function _callImmediately(fn, args, ctxt) {
            last = +(new Date());
            return fn.apply(ctxt, args);
        }

        return function () {
            ctxt = ctxt || this;
            clearTimeout(timer);
            if (last && (+(new Date())) < last + threshold) {
                timer = setTimeout(_callImmediately.bind(null, fn, arguments, ctxt), threshold);
            } else {
                _callImmediately(fn, arguments, ctxt);
            }
        }
    }

    function DomQuery(selector, parent) {
        if ('string' === typeof parent) {
            parent = document.getElementById(parent);
        }
        return [].slice.call((parent || document).querySelectorAll(selector));
    }

    degu = {
        PubSub: PubSub,
        partial: partial,
        compose: compose,
        throttle: throttle,
        debounce: debounce,
        memoize: memoize,
        DOM: {
            query: DomQuery
        }
    }

    return degu;

}());
